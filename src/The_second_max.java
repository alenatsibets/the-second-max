import java.util.Random;

public class The_second_max {
    public static void main(String[] args) {
    int[] array = new int[15];
    initialize(array);
    int max = max(array);
    int max2 = secondLargest(array, max);
    display(array);
    display(max);
    display(max2);
    }
    public static int max(int[] values) {
        int max = values[0];
        for (int value : values) {
            if (max < value) {
                max = value;
            }
        }
        return max;
    }
    public static void initialize(int[] arr) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(100);
        }
    }
    public static int secondLargest(int[] values, int max) {
        int max2 = values[0];
        for (int value : values) {
            if (value != max && max2 < value) {
                max2 = value;
            }
        }
        return max2;
    }
    public static void display(int a){
        System.out.println("The second-largest number in the array: " + a);
    }
    public static void display(int[] array){
        for (int value : array) {
            System.out.print(value + " ");
            }
        }

}